(ns calc.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [calc.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[calc started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[calc has shut down successfully]=-"))
   :middleware wrap-dev})
