(ns calc.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[calc started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[calc has shut down successfully]=-"))
   :middleware identity})
