-- Place your queries here. Docs available https://www.hugsql.org/

-- :name create-user! :! :n
-- :doc Create a new user, password will be hashed before storage
insert into users(username, password, active)
values(:username, crypt(:password, gen_salt('bf')), :active)


-- :name get-user :? :1
-- :doc Fetches a user by username and plaintext password.
select id, username, active
from users
where username = :username and password = crypt(:password, password) and active

-- :name get-user-balance :? :1
-- :doc Given a userid fetches their balance
select balance
from users
where id = :userid

-- :name set-user-balance! :! :n
-- :doc sets a users balance
update users
set balance = :balance
where id = :userid


-- :name deduct-user-balance! :! :1
-- :doc Deducts the users balance by cost, unless it would reduce the balance below zero, in which case it leaves the user unmodified. Returns the balance if the user was modifed or nil if the user lacked funds
update users
set balance = balance - :cost
where id = :userid and (balance - :cost) >= 0
returning balance;

-- :name record-operation! :! :n
-- :doc adds a record of the given operation to the records table
insert into records (user_id, operation, amount, user_balance, response)
values (:userid, :operation, :amount, :balance, :response)


-- :name find-records :? :*
select id, operation, amount, user_balance, response, created_at
from records
where user_id = :userid
and not deleted
/*~ (when (:search params) */
and (operation ilike :search or response ilike :search)
/*~ ) ~*/
/*~ (cond (and (:sort params) (:desc params)) */
order by :i:sort desc
/*~ (:sort params) */
order by :i:sort
/*~ :else */
order by created_at
/*~ ) ~*/
/*~ (when (:page-size params) */
limit :page-size
/*~ ) ~*/
/*~ (when (and (:page-size params) (:page params)) */
offset (:page - 1) * :page-size
/*~ ) ~*/

-- :name get-record-count :? :1
-- :doc gets the total record count for a given user
select count(*) as total
from records
where user_id = :userid and not deleted
/*~ (when (:search params) */
and (operation ilike :search or response ilike :search)
/*~ ) ~*/

-- :name delete-record! :! :n
-- :doc soft delete the record specified by id, if and only if it is owned by the user
update records
set deleted = true
where user_id = :userid and id = :id
