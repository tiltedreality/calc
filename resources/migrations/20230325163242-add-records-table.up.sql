CREATE TABLE records (
id BIGSERIAL PRIMARY KEY,
user_id BIGINT REFERENCES users,
operation VARCHAR(30),
amount INT,
user_balance BIGINT,
response VARCHAR(50),
created_at TIMESTAMP WITH TIME ZONE default now()
)
