# LoanPro Calculator

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

The recommended way of running the application for development is using the repl. First start the dev and testing databases via:

```sh
docker-compose up -d
```

Then launch a REPL via your editor of choice. [Cider/Emacs](https://docs.cider.mx/cider/basics/up_and_running.html), [VSCode/Calva](https://calva.io/connect/) or [IntelliJ/Cursive](https://cursive-ide.com/userguide/repl.html#local-repls).
From the `user` namespace (should be the default) you can then run:

```clojure
(start)
```

The server should now be running on http://localhost:3000.

If you want to start a server without a repl run:

```sh
lein run
```


## API

Swagger API Documentation can be found locally (http://localhost:3000) or on the live service (https://calc-api.tiltedreality.com). All requests with the exception of the `token` endpoint require a Bearer token in the authorization header in the form of `Bearer <token>`

## Implementation discussion

### Charging the user

When charging users performing the charge and performing the action is inherently racey, since these two things can't be done atomically. In deciding my approach I prioritized the following:

* We want the user to be able to perform simulatneous requests (and therefore should not lock excessively)
* The user should not be charged for failed requests
* We don't want a user with zero balance to be able to generate load on the system (therefore must check the balance up-front before performing the action)

To meet these constraints I elected for the following flow.

1. Before processing the request, check the users balance to make sure it's greater than the cost
2. Perform the requested action and obtain the result
3. Before sending the result, atomically ensure the balance is still sufficient (it could have dropped during procesing if the user is making simulatneous requests), deduct the balance, and record the operation.

This has the tradeoff that the user may be able to make us do some additional work they don't have the balance for if they are issuing concurrent requests, but their window to do so is limited. A possible option to prevent this would be to check and deduct the cost before performing the request, and attempt to refund the cost if the request fails, but has the tradeoff that refunding the request could fail (ex the network has gone down). We could also hold a serializable transaction open until the request is completed at the cost of additional locking.

In the end this is essentially an accounting system, and the real world the behaviors and contrainsts in these cases should be carefully considered to make sure the system has the desired properties.

### Authentication

Authentication is in the form of an encrypted JWT, which expires after 1 hour. Renewal has not been implemented but should be in a production system. The JWT has the tradeoff of not immediately being invalidated if a user is inactivated (without an additional database operation), but saves a database lookup at the start of a request.

### Random.org client

As [requested by random.org](https://www.random.org/clients/) the client is limited to a single thread and maintains a buffer of strings instead of performing a request for each string fetch, and includes my e-mail in the User-Agent. Quota checking has not been implemented.

### Data modeling

I did not see a benifit to making an operation a database entity since adding new operations would require new code and an application deploy anyway. In some scenarios this could make sense however, for example if we wanted to be able to update prices dynamically.

A `deleted` field was added to records to support the specified soft-deletes. I interpreted the `operation_response` field to be the outcome of the operation (success, failure) as opposed to the result of the operation.

A `balance` field was added to the users to hold the current balance.

### Deployment

For pragmatism's sake I deployed to infrastructure I already own. The full Kubernetes manifest can be found in `kube/deploy/calc-api.yml`. It is automatically built and deployed by the CI pipeline defined in `.gitlab-ci.yml`.
