---
apiVersion: v1
kind: Namespace
metadata:
  name: calc
---
apiVersion: postgresql.cnpg.io/v1
kind: Cluster
metadata:
  name: calc
  namespace: calc
spec:
  instances: 1
  primaryUpdateStrategy: unsupervised
  postgresql:
    parameters:
      shared_buffers: "256MB"
  storage:
    size: 50Gi
  resources:
    requests:
      memory: "1Gi"
      cpu: 500m
    limits:
      memory: "1Gi"
      cpu: 500m
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: calc-api
  namespace: calc
spec:
  selector:
    matchLabels:
      app: calc-api
  replicas: 1
  template:
    metadata:
      labels:
        app: calc-api
    spec:
      containers:
      - name: calc-api
        image: registry.gitlab.com/tiltedreality/calc/calc-api:{IMAGE_TAG}
        env:
          - name: DB_USER
            valueFrom:
              secretKeyRef:
                name: calc-app
                key: username
          - name: DB_PASS
            valueFrom:
              secretKeyRef:
                name: calc-app
                key: password
          - name: DB_HOST
            value: calc-rw
          - name: DB_PORT
            value: "5432"
          - name: DB_NAME
            value: app
          - name: SECRET_KEY_BASE #I don't belong here, but this is not a production app
            value: "OE30HjolmD1qg6s2IiFFMzTY7MjmP9VgHGe06ojYKvY="
        resources:
          limits:
            memory: "512Mi"
            cpu: "500m"
        ports:
        - containerPort: 3000
        livenessProbe:
          httpGet:
            path: /
            port: 3000
      imagePullSecrets: 
      - name: calc-gitlab-auth
---
apiVersion: v1
kind: Service
metadata:
  name: calc-api
  namespace: calc
spec:
  selector:
    app: calc-api
  ports:
  - port: 3000
    targetPort: 3000
---
apiVersion: k8s.nginx.org/v1
kind: VirtualServer
metadata:
  namespace: calc
  name: calc-api.tiltedreality.com
spec:
  host: calc-api.tiltedreality.com
  tls:
    secret: calc-api.tiltedreality.com.cert
    cert-manager:
      cluster-issuer: letsencrypt-prod
    redirect:
      enable: true
  externalDNS:
    enable: true
  upstreams:
  - name: calc-api
    service: calc-api
    port: 3000
  routes:
  - path: /
    action:
      pass: calc-api
---
apiVersion: batch/v1
kind: Job
metadata:
  namespace: calc
  name: migrations-{IMAGE_TAG}
spec:
  ttlSecondsAfterFinished: 3600
  template:
    spec:
      imagePullSecrets: 
      - name: calc-gitlab-auth
      containers:
      - name: migration
        image: registry.gitlab.com/tiltedreality/calc/calc-api:{IMAGE_TAG}
        command: ["/calc/run_migrations.sh"]
        resources:
          limits:
            memory: "512Mi"
            cpu: "125m"
        env:
          - name: DB_USER
            valueFrom:
              secretKeyRef:
                name: calc-app
                key: username
          - name: DB_PASS
            valueFrom:
              secretKeyRef:
                name: calc-app
                key: password
          - name: DB_HOST
            value: calc-rw
          - name: DB_PORT
            value: "5432"
          - name: DB_NAME
            value: app
          - name: SECRET_KEY_BASE
            value: "OE30HjolmD1qg6s2IiFFMzTY7MjmP9VgHGe06ojYKvY="
      restartPolicy: Never
