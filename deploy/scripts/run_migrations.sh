#!/usr/bin/env bash

set -Eeuo pipefail

export "DATABASE_URL=postgresql://$DB_HOST:$DB_PORT/$DB_NAME?user=$DB_USER&password=$DB_PASS"

java -jar /calc/app.jar migrate
