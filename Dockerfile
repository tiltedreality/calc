FROM eclipse-temurin:17-jre-jammy

COPY target/uberjar/calc.jar /calc/app.jar
COPY deploy/scripts/start_app.sh /calc/start_app.sh
COPY deploy/scripts/run_migrations.sh /calc/run_migrations.sh

EXPOSE 3000

CMD ["/calc/start_app.sh"]
