(ns calc.operations-test
  (:require
   [calc.config :refer [env]]
   [calc.db.core :as db]
   [calc.operations :as ops]
   [clojure.test :refer [deftest is testing use-fixtures]]
   [luminus-migrations.core :as migrations]
   [matcher-combinators.test]
   [mount.core :as mount]
   [next.jdbc :as jdbc]
   [next.jdbc.transaction :refer [*nested-tx*]]
   [calc.clients.random :as random])
  (:import
   [clojure.lang ExceptionInfo]))

(use-fixtures
  :each
  (fn [f]
    (mount/start
     #'calc.config/env
     #'calc.db.core/*db*)
    (migrations/migrate ["migrate"] (select-keys env [:database-url]))
    (jdbc/with-transaction [t-conn db/*db* {:rollback-only true}]
      (binding [db/*db* t-conn
                *nested-tx* :ignore]
        (db/create-user! db/*db* {:username "test@test.com" :password "test" :active true})
        (f)))))

(deftest costing-test
  (let [{:keys [id]} (db/get-user db/*db* {:username "test@test.com" :password "test"})]
    (testing "user has the funds"
      (db/set-user-balance! db/*db* {:userid id :balance 20})
      (is (= 5 (ops/costing 10 id "plus" (fn [] (+ 2 3)))))
      (is (= {:balance 10} (db/get-user-balance db/*db* {:userid id}))))

    (testing "User does not have the funds"
      (db/set-user-balance! db/*db* {:userid id :balance 5})
      (is (thrown-match? ExceptionInfo
                         {:reason :insufficient-balance}
                         (ops/costing 10 id "plus" (fn [] (+ 2 3)))))
      (is (= {:balance 5} (db/get-user-balance db/*db* {:userid id}))))

    (testing "User has sufficient funds, but funds are reduced before op completes"
      (db/set-user-balance! db/*db* {:userid id :balance 15})
      (is (thrown-match? ExceptionInfo
                         {:reason :insufficient-balance}
                         (ops/costing 10 id "plus"
                                      (fn []
                                        (db/set-user-balance! db/*db* {:userid id :balance 5})
                                        (+ 2 3)))))
      (is (= {:balance 5} (db/get-user-balance db/*db* {:userid id}))))))

(deftest plus-test
  (let [{:keys [id]} (db/get-user db/*db* {:username "test@test.com" :password "test"})]
    (testing "plus costs 1 and adds"
      (db/set-user-balance! db/*db* {:userid id :balance 20})
      (is (= 3 (ops/plus id 1 2)))
      (is (= {:balance 19} (db/get-user-balance db/*db* {:userid id})))
      (is (= 1.5 (ops/plus id 1 0.5)))
      (is (= -3 (ops/plus id 1 -4)))
      (is (= {:balance 17} (db/get-user-balance db/*db* {:userid id}))))))

(deftest minus-test
  (let [{:keys [id]} (db/get-user db/*db* {:username "test@test.com" :password "test"})]
    (testing "minus costs 2 and subtracts"
      (db/set-user-balance! db/*db* {:userid id :balance 20})
      (is (= 1 (ops/minus id 2 1)))
      (is (= {:balance 18} (db/get-user-balance db/*db* {:userid id})))
      (is (= 1.5 (ops/minus id 2 0.5)))
      (is (= -3 (ops/minus id 1 4)))
      (is (= {:balance 14} (db/get-user-balance db/*db* {:userid id}))))))

(deftest multiply-test
  (let [{:keys [id]} (db/get-user db/*db* {:username "test@test.com" :password "test"})]
    (testing "multiply costs 3 and multiplies"
      (db/set-user-balance! db/*db* {:userid id :balance 20})
      (is (= 2 (ops/multiply id 2 1)))
      (is (= {:balance 17} (db/get-user-balance db/*db* {:userid id})))
      (is (= 1.0 (ops/multiply id 2 0.5)))
      (is (= -8 (ops/multiply id 2 -4)))
      (is (= {:balance 11} (db/get-user-balance db/*db* {:userid id}))))))

(deftest divide-test
  (let [{:keys [id]} (db/get-user db/*db* {:username "test@test.com" :password "test"})]
    (testing "divide costs 4 and divides"
      (db/set-user-balance! db/*db* {:userid id :balance 20})
      (is (= 2 (ops/divide id 2 1)))
      (is (= {:balance 16} (db/get-user-balance db/*db* {:userid id})))
      (is (= 4.0 (ops/divide id 2 0.5)))
      (is (= -2 (ops/divide id -4 2)))
      (is (= {:balance 8} (db/get-user-balance db/*db* {:userid id}))))))

(deftest root-test
  (let [{:keys [id]} (db/get-user db/*db* {:username "test@test.com" :password "test"})]
    (testing "root costs 5 and takes the square root"
      (db/set-user-balance! db/*db* {:userid id :balance 20})
      (is (= 2.0 (ops/root id 4)))
      (is (= {:balance 15} (db/get-user-balance db/*db* {:userid id})))
      (is (= 3.0 (ops/root id 9)))
      (is (NaN? (ops/root id -1)))
      (is (= {:balance 5} (db/get-user-balance db/*db* {:userid id}))))))

(deftest strings-test
  (let [{:keys [id]} (db/get-user db/*db* {:username "test@test.com" :password "test"})]
    (with-redefs [random/get-random-string (fn []
                                             ; chosen via random dice roll
                                             "4")]
      (testing "random-string costs 10 and gets a string"
        (db/set-user-balance! db/*db* {:userid id :balance 20})
        (is (= "4" (ops/random-string id)))
        (is (= {:balance 10} (db/get-user-balance db/*db* {:userid id})))))))
