(ns calc.handler-test
  (:require
   [calc.config :as config]
   [calc.handler :refer [app]]
   [calc.operations :as ops]
   [calc.test-utils :refer [authenticated-request]]
   [clojure.test :refer [deftest is testing use-fixtures]]
   [mount.core :as mount]
   [muuntaja.core :as m]
   [ring.mock.request :refer [body content-type header request]]))

(use-fixtures
  :once
  (fn [f]
    (mount/start #'calc.config/env
                 #'calc.handler/app-routes)
    (with-redefs [ops/costing (fn [_ _ _ op] (op))]
      (f))))

(deftest test-app
  (testing "main route"
    (let [response ((app) (request :get "/"))]
      (is (= 301 (:status response)))))

  (testing "not-found route"
    (let [response ((app) (request :get "/invalid"))]
      (is (= 404 (:status response)))))
  (testing "services"

    (testing "success"
      (let [response ((app) (authenticated-request :post "/api/v1/math/plus" {:x 10, :y 6}))]
        (is (= 200 (:status response)))
        (is (= {:total 16} (m/decode-response-body response)))))

    (testing "parameter coercion error"
      (let [response ((app) (authenticated-request :post "/api/v1/math/plus" {:x 10, :y "invalid"}))]
        (is (= 400 (:status response)))))

    (testing "content negotiation"
      (let [response ((app) (-> (authenticated-request :post "/api/v1/math/plus")
                                (body (pr-str {:x 10, :y 6}))
                                (content-type "application/edn")
                                (header "accept" "application/transit+json")))]
        (is (= 200 (:status response)))
        (is (= {:total 16} (m/decode-response-body response)))))))
