(ns calc.clients.random-test
  (:require
   [clojure.test :refer [deftest is testing use-fixtures]]
   [calc.clients.random :as random])
  (:import
   [clojure.lang PersistentQueue]))

(use-fixtures
  :each
  (fn [f]
    (reset! random/strings (PersistentQueue/EMPTY))
    (with-redefs [random/fetch-strings (fn [] (->> (range 1 11)
                                                   (map str)
                                                   vec))]
      (f))))

(deftest get-random-string-test
  (testing "fetches new strings and populates buffer when buffer empty"
    (is (empty? @random/strings))
    (is (= "10" (random/get-random-string)))
    (is (= 9 (count  @random/strings))))

  (testing "does not fetch new strings when available in buffer"
    (is (seq @random/strings))

    (let [prev-count (count @random/strings)]
      (with-redefs [random/fetch-strings (fn [] (is false "fetch-strings should not be called"))]
        (is (= "1" (random/get-random-string)))
        (is (= (dec prev-count) (count @random/strings)))))))
