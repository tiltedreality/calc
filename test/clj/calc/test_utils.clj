(ns calc.test-utils
  (:require
   [ring.mock.request :refer [json-body request]]))

(defn authenticated-request
  ([method path] (authenticated-request method path nil))
  ([method path body]
   (-> (request method path)
       (json-body body)
       (assoc :user {:username "test@test.com" :active true}))))
