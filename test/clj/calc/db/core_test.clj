(ns calc.db.core-test
  (:require
   [calc.db.core :refer [*db*] :as db]
   [java-time.pre-java8]
   [luminus-migrations.core :as migrations]
   [clojure.test :refer [use-fixtures deftest is testing]]
   [next.jdbc :as jdbc]
   [calc.config :refer [env]]
   [mount.core :as mount]
   [matcher-combinators.test]
   [matcher-combinators.matchers :as m]))

(use-fixtures
  :once
  (fn [f]
    (mount/start
     #'calc.config/env
     #'calc.db.core/*db*)
    (migrations/migrate ["migrate"] (select-keys env [:database-url]))
    (f)))

(deftest test-users
  (testing "user creation and fetching"
    (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
      (is (= 1 (db/create-user!
                t-conn
                {:username "test@test.com"
                 :password "test"
                 :active true}
                {})))
      (is (match? {:username "test@test.com"
                   :active true}
                  (db/get-user t-conn {:username "test@test.com" :password "test"} {})))
      (is (nil? (db/get-user t-conn {:username "test@test.com" :password "wrong"} {})))
      (is (nil? (db/get-user t-conn {:username "notest@test.com" :password "test"} {})))))
  (testing "user balance functions"
    (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
      (is (= 1 (db/create-user!
                t-conn
                {:username "test@test.com"
                 :password "test"
                 :active true}
                {})))
      (let [{:keys [id]} (db/get-user t-conn {:username "test@test.com" :password "test"})]
        (is (= 1 (db/set-user-balance! t-conn {:userid id :balance 20})))
        (is (= {:balance 20} (db/get-user-balance t-conn {:userid id})))

        (testing "deducting a users balance"
          (testing "when they have the funds"
            (is (= {:balance 10} (db/deduct-user-balance! t-conn {:userid id :cost 10})))
            (is (= {:balance 10} (db/get-user-balance t-conn {:userid id}))))

          (testing "when they are out of funds"
            (is (nil? (db/deduct-user-balance! t-conn {:userid id :cost 15})))
            (is (= {:balance 10} (db/get-user-balance t-conn {:userid id})))))))))


(deftest test-records
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    (is (= 1 (db/create-user! t-conn {:username "test@test.com" :password "test" :active true})))
    (is (= 1 (db/create-user! t-conn {:username "other@test.com" :password "other" :active true})))
    (let [{:keys [id]} (db/get-user t-conn {:username "test@test.com" :password "test"})
          otherid (:id (db/get-user t-conn {:username "other@test.com" :password "other"}))]

      (testing "record creating"
        (is (= 1 (db/record-operation! t-conn {:userid id :operation "test" :amount 1 :balance 9 :response "OK"})))
        (is (= 1 (db/record-operation! t-conn {:userid id :operation "test2" :amount 2 :balance 8 :response "BAD"}))))

      (testing "fetching all user records"
        (is (match? (m/in-any-order [{:operation "test" :response "OK"}
                                     {:operation "test2" :response "BAD"}])
                    (db/find-records t-conn {:userid id}))))

      (testing "searching user records by operation"
        (is (match? [{:operation "test2"}] (db/find-records t-conn {:userid id :search "%test2%"})))
        (is (match? [{:operation "test2"}] (db/find-records t-conn {:userid id :search "%st2%"})))
        (is (match? (m/in-any-order [{:operation "test2"} {:operation "test"}]) (db/find-records t-conn {:userid id :search "%st%"}))))

      (testing "searching user records by response"
        (is (match? [{:response "OK"}] (db/find-records t-conn {:userid id :search "%ok%"})))
        (is (match? [{:response "BAD"}] (db/find-records t-conn {:userid id :search "%ad%"})))
        (is (match? (m/in-any-order [{:operation "test2"} {:operation "test"}]) (db/find-records t-conn {:userid id :search "%st%"}))))

      (testing "sorting user records"
        (is (match? [{:operation "test"} {:operation "test2"}]
                    (db/find-records t-conn {:userid id :sort "operation"})))
        (is (match? [{:operation "test2"} {:operation "test"}]
                    (db/find-records t-conn {:userid id :sort "operation" :desc true})))

        (is (match? [{:user_balance 8} {:user_balance 9}]
                    (db/find-records t-conn {:userid id :sort "user_balance"})))
        (is (match? [{:user_balance 9} {:user_balance 8}]
                    (db/find-records t-conn {:userid id :sort "user_balance" :desc true})))

        (is (match? [{:response "BAD"} {:response "OK"}]
                    (db/find-records t-conn {:userid id :sort "response"})))
        (is (match? [{:response "OK"} {:response "BAD"}]
                    (db/find-records t-conn {:userid id :sort "response" :desc true}))))

      (testing "paging"
        (is (match? [{:operation "test"}]
                    (db/find-records t-conn {:userid id :sort "operation" :page-size 1})))
        (is (match? [{:operation "test"}]
                    (db/find-records t-conn {:userid id :sort "operation" :page-size 1 :page 1})))
        (is (match? [{:operation "test2"}]
                    (db/find-records t-conn {:userid id :sort "operation" :page-size 1 :page 2})))
        (is (match? []
                    (db/find-records t-conn {:userid id :sort "operation" :page-size 1 :page 3}))))

      (testing "can't access other users records"
        (is (match? [] (db/find-records t-conn {:userid otherid}))))

      (testing "deleting records"
        (let [to-delete (-> (db/find-records t-conn {:userid id :search "OK"}) first :id)]
          (is (not (nil? to-delete)))
          (is (= 1 (db/delete-record! t-conn {:userid id :id to-delete})))
          (is (match? [] (db/find-records t-conn {:userid id :search "OK"})))

          (testing "records are soft-deleted only"
            (is (match? [{:records/response "OK" :records/deleted true}] (jdbc/execute! t-conn ["select * from records where id=?" to-delete])))))

        (testing "can't delete someone else's record"
          (let [to-delete (-> (db/find-records t-conn {:userid id :search "BAD"}) first :id)]
            (is (not (nil? to-delete)))
            (is (zero? (db/delete-record! t-conn {:userid otherid :id to-delete})))
            (is (match? [{:response "BAD"}] (db/find-records t-conn {:userid id :search "BAD"})))))))))
