(ns calc.middleware
  (:require
   [buddy.core.nonce :refer [random-bytes]]
   [buddy.sign.jwt :refer [decrypt encrypt]]
   [buddy.sign.util :refer [to-timestamp]]
   [calc.env :refer [defaults]]
   [calc.config :refer [env]]
   [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
   [ring.util.http-response :refer [payment-required unauthorized]]
   [mount.core :refer [defstate]])
   (:import
    [clojure.lang ExceptionInfo]
    [java.util Calendar Date Base64]))

(defstate secret
  :start (if-let [secret-base (env :secret-key-base)]
           (.decode (Base64/getDecoder) secret-base)
           (throw (Exception. "Please define a secret-key-base"))))

(def jwt-opts {:alg :a256kw
               :enc :a128gcm})

(defn wrap-restricted [handler]
  (fn [request]
    (if (:user request)
      (handler request)
      (unauthorized (:error "User does not exist or token is invalid")))))

(defn token [user]
  (let [claims {:user user
                :exp (to-timestamp
                      (.getTime
                       (doto (Calendar/getInstance)
                         (.setTime (Date.))
                         (.add Calendar/HOUR_OF_DAY 1))))}]
    (encrypt claims secret jwt-opts)))

(defn wrap-decode-jwe [handler]
  (fn [request]
    (try
      (let [auth-header (get-in request [:headers "authorization"])
            [_ token] (when auth-header (re-matches #"^Bearer (.*)$" auth-header))
            {:keys [user]} (when token (decrypt token secret {:alg :a256kw :enc :a128gcm}))]
        (if user
          (handler (assoc request :user user))
          (handler request)))
      (catch ExceptionInfo e
        (handler (assoc request :jwe-error e))))))

(defn wrap-handle-insufficient-balance [handler]
  (fn [request]
    (try
      (handler request)
      (catch ExceptionInfo e
        (let [{:keys [reason]} (ex-data e)]
          (if (= :insufficient-balance reason)
            (payment-required)
            (throw e)))))))

(defn wrap-base [handler]
  (-> ((:middleware defaults) handler)
      wrap-decode-jwe
      (wrap-defaults
       (-> site-defaults
           (assoc-in [:security :anti-forgery] false)))))


