(ns calc.routes.services
  (:require
   [calc.db.core :as db]
   [calc.middleware :refer [token wrap-handle-insufficient-balance
                            wrap-restricted]]
   [calc.middleware.formats :as formats]
   [calc.operations :as ops]
   [reitit.coercion.spec :as spec-coercion]
   [reitit.ring.coercion :as coercion]
   [reitit.ring.middleware.multipart :as multipart]
   [reitit.ring.middleware.muuntaja :as muuntaja]
   [reitit.ring.middleware.parameters :as parameters]
   [reitit.swagger :as swagger]
   [reitit.swagger-ui :as swagger-ui]
   [ring.middleware.cors :refer [wrap-cors]]
   [ring.util.http-response :refer [bad-request ok unauthorized]]
   [spec-tools.data-spec :as ds]))

(defn cors [handler]
  (wrap-cors handler
             :access-control-allow-origin [#"https://calc.tiltedreality.com"]
             :access-control-allow-methods [:get :put :post :delete]))

(defn service-routes []
  ["/api"
   {:coercion spec-coercion/coercion
    :muuntaja formats/instance
    :swagger {:id ::api}
    :middleware [ ;; query-params & form-params
                 parameters/parameters-middleware
                 ;; content-negotiation
                 muuntaja/format-negotiate-middleware
                 ;; encoding response body
                 muuntaja/format-response-middleware
                 ;; exception handling
                 coercion/coerce-exceptions-middleware
                 ;; decoding request body
                 muuntaja/format-request-middleware
                 ;; coercing response bodys
                 coercion/coerce-response-middleware
                 ;; coercing request parameters
                 coercion/coerce-request-middleware
                 ;; multipart
                 multipart/multipart-middleware
                 cors]}

   ;; swagger documentation
   ["" {:no-doc true
        :swagger {:info {:title "Calculator API"}}}

    ["/swagger.json"
     {:get (swagger/create-swagger-handler)}]

    ["/api-docs/*"
     {:get (swagger-ui/create-swagger-ui-handler
            {:url "/api/swagger.json"
             :config {:validator-url nil}})}]]

   ["/v1"

    ["/token"
     {:post {:summary "provides authentication token for the given username/password, must be provided in the Authorization header as a Bearer token for future requests in the form 'Bearer <token>'"
             :parameters {:body {:username string? :password string?}}
             :responses {200 {:body {:token string?}}
                         401 {}}
             :handler (fn [request]
                        (let [{:keys [username password]} (:body-params request)]
                          (if-let [user (db/get-user db/*db* {:username username :password password})]
                            (ok {:token (token user)})
                            (unauthorized))))}}]

    ["/user"
     {:swagger {:tags ["user"]}
      :middleware [wrap-restricted]}

     ["/balance"
      {:get {:summary "gets the users token balance"
             :responses {200 {:body {:balance int?}}}
             :handler (fn [request]
                        (let [id (get-in request [:user :id])
                              balance (:balance (db/get-user-balance {:userid id}))]
                          (ok {:balance balance})))}

       :post {:summary "sets the users token balance (for ease of evaluation)"
              :parameters {:body {:balance pos-int?}}
              :responses {200 {:body nil}}
              :handler (fn [request]
                         (let [balance (get-in request [:body-params :balance])
                               id (get-in request [:user :id])]
                           (db/set-user-balance! {:userid id :balance balance})
                           (ok)))}}]
     ["/record/:id"
      {:delete {:summary "delete the record with the given id"
                :parameters {:path {:id int?}}
                :responses {200 {:body nil}
                            400 {:body {:error string?}}}
                :handler (fn [request]
                           (let [id (get-in request [:user :id])
                                 record-id (get-in request [:parameters :path :id])]
                             (if (= 1 (db/delete-record! {:userid id :id record-id}))
                               (ok)
                               (bad-request {:error "record does not exist"}))))}}]

     ["/records"
      {:get {:summary "gets all user records"
             :parameters {:query {(ds/opt :search) string?
                                  (ds/opt :sort) string?
                                  (ds/opt :desc) boolean?
                                  (ds/opt :page-size) pos-int?
                                  (ds/opt :page) nat-int?}}
             :responses {200 {:body {:pages nat-int?
                                     :records
                                     [{:id int?
                                       :operation string?
                                       :amount int?
                                       :user_balance int?
                                       :response string?
                                       :created_at inst?}]}}}
             :handler (fn [request]
                        (let [id (get-in request [:user :id])
                              {:keys [search sort desc page-size page]} (get-in request [:parameters :query])]
                          (ok (ops/get-records id search sort desc page-size page))))}}]]

    ["/string"
     {:swagger {:tags ["string"]}
      :middleware [wrap-restricted wrap-handle-insufficient-balance]}

     ["/random"
      {:post {:summary "generates a random string of length 10, consisting of 1-9, a-z, A-Z, costs 10 tokens"
              :responses {200 {:body {:result string?}}}
              :handler (fn [request]
                         (let [id (get-in request [:user :id])]
                           {:status 200
                            :body {:result (ops/random-string id)}}))}}]]

    ["/math"
     {:swagger {:tags ["math"]}
      :middleware [wrap-restricted wrap-handle-insufficient-balance]}

     ["/plus"
      {:post {:summary "adds x and y, costs 1 token"
              :parameters {:body {:x number?, :y number?}}
              :responses {200 {:body {:total number?}}}
              :handler (fn [request]
                         (let [{:keys [x y]} (:body-params request)
                               id (get-in request [:user :id])]
                           {:status 200
                            :body {:total (ops/plus id x y)}}))}}]

     ["/minus"
      {:post {:summary "subtracts y from x (x - y), costs 2 tokens"
              :parameters {:body {:x number?, :y number?}}
              :responses {200 {:body {:total number?}}}
              :handler (fn [request]
                         (let [{:keys [x y]} (:body-params request)
                               id (get-in request [:user :id])]
                           {:status 200
                            :body {:total (ops/minus id x y)}}))}}]

     ["/multiply"
      {:post {:summary "multiplies x and y, costs 3 tokens"
              :parameters {:body {:x number?, :y number?}}
              :responses {200 {:body {:total number?}}}
              :handler (fn [request]
                         (let [{:keys [x y]} (:body-params request)
                               id (get-in request [:user :id])]
                           {:status 200
                            :body {:total (ops/multiply id x y)}}))}}]

     ["/divide"
      {:post {:summary "computes x over y (x / y), costs 4 tokens"
              :parameters {:body {:x number?, :y number?}}
              :responses {200 {:body {:total number?}}}
              :handler (fn [request]
                         (let [{:keys [x y]} (:body-params request)
                               id (get-in request [:user :id])]
                           {:status 200
                            :body {:total (ops/divide id x y)}}))}}]

     ["/root"
      {:post {:summary "computes the square root of x, costs 5 tokens"
              :parameters {:body {:x number?}}
              :responses {200 {:body {:total number?}}}
              :handler (fn [request]
                         (let [{:keys [x]} (:body-params request)
                               id (get-in request [:user :id])]
                           {:status 200
                            :body {:total (ops/root id x)}}))}}]]]])
