(ns calc.clients.random
  (:require [hato.client :as hato]
            [clojure.string :as string])
  (:import [java.util.concurrent Executors]
           [clojure.lang PersistentQueue]))

(def strings (atom (PersistentQueue/EMPTY)))

(def client (hato/build-http-client {:executor (Executors/newSingleThreadExecutor)}))

(defn fetch-strings []
  (-> (hato/get "https://www.random.org/strings/"
                   {:http-client client
                    :query-params {:num 20
                                   :len 10
                                   :digits "on"
                                   :upperalpha "on"
                                   :loweralpha "on"
                                   :unique "off"
                                   :format "plain"
                                   :rnd "new"}
                    :headers {"user-agent" "Calc <me@waltonhoops.com>"}})
      :body
      string/split-lines))

(defn- fetch-new-strings []
  (let [lines (fetch-strings)]
    (apply swap! strings conj (pop lines))
    (peek lines)))

(defn get-random-string []
  (let [[old _] (swap-vals! strings pop)
        string (peek old)]
    (if string
      string
      (fetch-new-strings))))
