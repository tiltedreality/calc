(ns calc.operations
  (:require [calc.db.core :as db]
            [calc.clients.random :as random]
            [next.jdbc :as jdbc])
  (:import [java.lang Math]))

(defn- insufficient-balance [cost userid op-name balance]
  (db/record-operation! db/*db* {:userid userid
                                 :amount cost
                                 :operation op-name
                                 :balance balance
                                 :response "Insufficient balance"})
  (throw (ex-info "Insufficient balance" {:reason :insufficient-balance})))

(defn- charge-operation [cost userid op-name]
  (if-let [{:keys [balance]} (db/deduct-user-balance! db/*db* {:userid userid :cost cost})]
    (db/record-operation! db/*db* {:userid userid
                                   :amount cost
                                   :operation op-name
                                   :balance balance
                                   :response "Success"})
    (insufficient-balance cost userid op-name (:balance (db/get-user-balance db/*db* {:userid userid})))))

(defn costing [cost userid op-name op]
  (let [{:keys [balance]} (db/get-user-balance db/*db* {:userid userid})]
    (when (< balance cost)
      (insufficient-balance cost userid op-name balance))
    (try
      (let [result (op)]
        (jdbc/with-transaction [t-conn db/*db*]
          (binding [db/*db* t-conn]
            (charge-operation cost userid op-name)))
        result)
      (catch Exception e
        (db/record-operation! db/*db* {:userid userid
                                       :amount cost
                                       :operation op-name
                                       :balance balance
                                       :response "System Error"})
        (throw e)))))


(defn plus [userid x y]
  (costing 1 userid "plus" (fn [] (+ x y))))

(defn minus [userid x y]
  (costing 2 userid "minus" (fn [] (- x y))))

(defn multiply [userid x y]
  (costing 3 userid "multiply" (fn [] (* x y))))

(defn divide [userid x y]
  (costing 4 userid "divide" (fn [] (/ x y))))

(defn root [userid x]
  (costing 5 userid "root" (fn [] (Math/sqrt x))))

(defn random-string [userid]
  (costing 10 userid "random-string" (fn [] (random/get-random-string))))

(def allowed-sort-cols #{"operation" "amount" "user_balance" "response" "created_at"})

(defn get-records [userid search sort desc page-size page]
  (let [{:keys [total]} (db/get-record-count db/*db* {:userid userid :search (str "%" search "%")})]
    {:pages (long (Math/ceil (/ total page-size)))
     :records (db/find-records {:userid userid
                                :search (str "%" search "%")
                                :sort (allowed-sort-cols sort)
                                :desc desc
                                :page-size page-size
                                :page page})}))
